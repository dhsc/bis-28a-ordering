<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    UBL syntax binding to the T76 for NHS
    Author: Celeris Group, Roberto Cisternino
    Timestamp: 2018-04-15 16:27:00
-->
<schema xmlns="http://purl.oclc.org/dsdl/schematron" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:UBL="urn:oasis:names:specification:ubl:schema:xsd:OrderResponse-2" queryBinding="xslt2">
	<title>BIIRULES  T76 bound to UBL</title>
	<ns prefix="cbc" uri="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"/>
	<ns prefix="cac" uri="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"/>
	<ns prefix="ubl" uri="urn:oasis:names:specification:ubl:schema:xsd:OrderResponse-2"/>
	<ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	
    <phase id="NHS_CheckOrderResponseModel_phase">
        <active pattern="NHS-CheckResponseModel"/>
	</phase>
	
	
	<!-- NHS rules -->
	<!-- ======================== -->
	<include href="NHS/NHS-UBL-T76.sch"/>
</schema>
