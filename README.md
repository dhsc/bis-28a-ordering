# README #



### What is this repository for? ###


	
	This repository stores Customized PEPPOL BIS and their Validation Artifacts for the UK Department of Health and Social Care



### How do I get set up? ###


* Configuration

	Validation Artifacts are in both XSLT and Schematron
	
	
* Dependencies


	UBL Schema validation (xsd) should be run first.
	
	XSLT can then run as standalone.  
	
	
	If you prefer to use schematron you should consider that the whole set of schematron files (validation layers) is made up by several artifacts, based on CEN BII and PEPPOL methodology.  On the other hand XSLT is a single file and may be a better solution for run-time.
	If using Schematron validation you should include the following schematron checks in advance;
	
		1. BII Rules
		
		2. OpenPEPPOL Rules
		
		3. In the case of Order Response you need to replace the codelist that is extended



### Contribution guidelines ###

### Who do I collaborate with? ###

* Repo owner or admin
